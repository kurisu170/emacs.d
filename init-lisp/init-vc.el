;;; init-vc.el --- Version control support -*- lexical-binding: t -*-
;;; Commentary:

;; Most version control packages are configured separately: see
;; init-git.el, for example.

;;; Code:
(use-package git-gutter
  :diminish
  :custom (git-gutter:update-interval 2)
  :config (global-git-gutter-mode))
(provide 'init-vc)
;;; init-vc.el ends here
