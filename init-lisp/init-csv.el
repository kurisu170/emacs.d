;;; init-csv.el --- CSV files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package csv-mode
  :defer t
  :custom (csv-separators '("," ";" "|" " ")))


(provide 'init-csv)
;;; init-csv.el ends here
