;;; init-shortcuts.el --- Shortcuts for various emacs commands -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package crux
  :ensure t)

(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)


(provide 'init-shortcuts)
;;; init-shortcuts.el ends here
