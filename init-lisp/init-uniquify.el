;;; init-uniquify.el --- Configure uniquification of buffer names  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

(provide 'init-uniquify)
;;; init-uniquify.el ends here
