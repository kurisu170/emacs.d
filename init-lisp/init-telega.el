;;; init-telega.el --- Telega configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package all-the-icons
  :ensure t)

(use-package telega
  :bind ("C-c t" . telega))


(provide 'init-telega)
;;; init-telega.el ends here
