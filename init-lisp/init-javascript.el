;;; init-javascript.el --- Support for Javascript and derivatives -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require-package 'json-mode)
(require-package 'typescript-mode)
(require-package 'prettier-js)

;;; Basic js-mode setup
(add-to-list 'auto-mode-alist '("\\.\\(js\\|es6\\)\\(\\.erb\\)?\\'" . js-mode))

;; js2-mode
(use-package js2-mode
  :defer t
  :interpreter ("node" . js2-node)
  :custom ((js-indent-level 2)
           ;; Disable js2 mode's syntax error highlighting by default...
           (js2-mode-show-parse-errors nil)
           (js2-mode-show-strict-warnings nil))
  :config
  ;; Enable rg or ag if present
  (when (and (or (executable-find "rg") (executable-find "ag"))
             (require-package 'xref-js2))
    (when (executable-find "rg")
      (setq-default xref-js2-search-program 'rg))
    (defun sanityinc/enable-xref-js2 ()
      (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t))
    (with-eval-after-load 'js
      (define-key js-mode-map (kbd "M-.") nil)
      (add-hook 'js-mode-hook 'sanityinc/enable-xref-js2))
    (with-eval-after-load 'js2-mode
      (define-key js2-mode-map (kbd "M-.") nil)
      (add-hook 'js2-mode-hook 'sanityinc/enable-xref-js2))))

;;; Coffeescript
(use-package coffee-mode
  :defer t
  :custom (cofee-tab-width js-indent-level)
  :mode "\\.coffee\\.erb\\'")

;; ---------------------------------------------------------------------------
;; Run and interact with an inferior JS via js-comint.el
;; ---------------------------------------------------------------------------

(use-package js-comint
  :defer t
  :hook (comint-output-filter-functions . js-comint-process-output)
  :custom (js-comint-program-command "node"))


(use-package add-node-modules-path
  :defer t
  :hook ((typescript-mode
          js-mode
          js2-mode
          coffee-mode) . add-node-modules-path))


(provide 'init-javascript)
;;; init-javascript.el ends here
