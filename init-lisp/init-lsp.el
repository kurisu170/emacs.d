;;; init-lsp.el --- Short description -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package eglot
  :hook (python-mode . eglot-ensure))
(provide 'init-lsp)
;;; init-lsp.el ends here
