;;; init-evil.el --- Evil configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'selectrum)
(require 'transient)

(use-package evil
  :defer nil
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-want-C-u-scroll t)
  :bind (("<escape>" . keyboard-escape-quit)
         :map evil-normal-state-map
         ("C-e" . move-end-of-line)
         ("M-." . xref-find-definitions)
         ;; Esc cancels magit prompt
         :map transient-base-map
         ("<escape>" . transient-quit-one)
         ;; Map M-j and M-k instead of C-n and C-p
         :map selectrum-minibuffer-map
         ("M-j" . selectrum-next-candidate)
         ("M-k" . selectrum-previous-candidate)
         :map comint-mode-map
         ("M-j" . comint-next-input)
         ("M-k" . comint-previous-input))
  :config
  (evil-mode))

(use-package evil-commentary
  :after evil
  :diminish
  :config (evil-commentary-mode))

(use-package evil-surround
  :after evil
  :diminish
  :config
  (global-evil-surround-mode))

(use-package evil-collection
  :after evil
  :custom
  (evil-collection-outline-bind-tab-p nil)
  :config
  (evil-collection-init))

(use-package evil-escape
  :diminish
  :after evil
  :custom ((evil-escape-key-sequence "jk")
           (evil-escape-unordered-key-sequence t))
  :config (evil-escape-mode))

;; General
(use-package general
  :config
  (general-create-definer maps/leader-key-def
    :states 'normal
    :prefix "SPC"
    :global-prefix "C-SPC")
    (general-evil-setup t))

(maps/leader-key-def
  ;; File operations
  "SPC" '(execute-extended-command :which-key "M-x")
  "f"   'find-file

  ;; Buffer operations
  "b"  'switch-to-buffer
  "B"  'ibuffer
  "k"  'kill-buffer

  ;; Dired
  "d"  'dired-jump
  "D"  'dired
  "4d"  'dired-jump-other-window
  "4D"  'dired-other-window

  ;; Window operations
  "1"  'delete-other-windows
  "3"  'split-window-right
  "2"  'split-window-below
  "W"  'delete-window
  "w"  'delete-other-windows

  ;; Help
  "h"   '(:ignore t :which-key "help")
  "h"   help-map

  ;; Evaluation
  "e"  'eval-last-sexp
  "E"  'eval-buffer

  ;; Run
  "s"  'shell

  ;; Org capture
  "c"  'org-capture

  ;; Git operations
  "g"  'magit-status
  "G"  'magit-dispatch

  ;; Projectile commands
  "p"  'projectile-find-file
  "Ps" 'projectile-switch-project
  "Pf" 'projectile-ripgrep
  "Pc" 'projectile-compile-project
  "Pd" 'projectile-dired

  ;; Register commands
  "r"  (general-simulate-key "C-x r" :which-key "registers")

  ;; Abbrev commands
  "a"  'inverse-add-global-abbrev
  "A"  'inverse-add-mode-abbrev

  ;; Adjust text scale
  "z" 'text-scale-adjust

  ;; Telega
  "t"  '(:ignore t :which-key "telega")
  "t"  telega-prefix-map)


(provide 'init-evil)
;;; init-evil.el ends here
