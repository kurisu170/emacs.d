;;; init-github.el --- Integration with git forges -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package forge
  :after magit)


(provide 'init-forge)
;;; init-forge.el ends here
