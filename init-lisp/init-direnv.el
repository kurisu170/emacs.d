;;; init-direnv.el --- Integrate with direnv -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package envrc
  :defer nil
  :if (executable-find "direnv")
  :bind-keymap ("C-c e" . envrc-command-map)
  :config (envrc-global-mode))


(provide 'init-direnv)
;;; init-direnv.el ends here
