;;; init-python.el --- Python editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:
(require 'project)
(use-package pip-requirements)

(use-package toml-mode
  :mode "poetry\\.lock\\'")

(defun py-isort-if-setupcfg-p ()
  "Sort buffer using py-isort if setup.cfg is present."
  (if (file-readable-p (expand-file-name "setup.cfg"
                                         (project-root (current-project))))
      (py-isort-before-save)))

(use-package py-isort
  :hook (before-save . py-isort-if-setupcfg-p))


(provide 'init-python)
;;; init-python.el ends here
