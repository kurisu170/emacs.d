;;; init-company.el --- Completion with company -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package company
  :defer nil
  :diminish
  :bind ((:map company-mode-map
               ("M-/" . company-complete-selection)
               :map company-active-map
               ("RET" . nil)
               ("<return>" . nil)
               ("TAB" . nil)

               ("M-n" . company-select-next)
               ("M-p" . company-select-previous)
               ("M-d" . company-show-doc-buffer)
               ("M-." . company-show-location)))
  :custom ((company-dabbrev-other-buffers 'all)
           (company-tooltip-align-annotations t)
           (company-minimum-prefix-length 1)
           (company-idle-delay 0)
           (company-selection-wrap-around t))
  :config
  (global-company-mode)
  (add-to-list 'completion-styles 'initials t)
  (add-to-list 'company-backends 'company-files))

(provide 'init-company)
;;; init-company.el ends here
