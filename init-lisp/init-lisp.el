;;; init-lisp.el --- Common config for all lisps -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; Activate paredit in lisp modes
(require 'paredit)
(dolist (mode '(emacs-lisp-mode-hook
               ielm-mode-hook
               lisp-mode-hook
               lisp-interactions-mode-hook
               scheme-mode-hook))
        (add-hook mode 'enable-paredit-mode))


(defun sanityinc/insert-elisp-header ()
  "Insert minimal header and footer to an elisp buffer."
  (interactive)
  (let ((fname (if (buffer-file-name)
                   (file-name-nondirectory (buffer-file-name))
                 (error "This buffer is not visiting a file"))))
    (save-excursion
      (goto-char (point-min))
      (insert ";;; " fname " --- Short description -*- lexical-binding: t -*-\n"
              ";;; Commentary:\n"
              ";;; Code:\n\n")
      (goto-char (point-max))
      (insert ";;; " fname " ends here\n"))))

;; Make C-x C-e run 'eval-region if the region is active
(defun sanityinc/eval-last-sexp-or-region (prefix)
  "Eval region from BEG to END if active, otherwise the last sexp."
  (interactive "P")
  (if (and (mark) (use-region-p))
      (eval-region (min (point) (mark)) (max (point) (mark)))
    (eval-last-sexp prefix)))

  (global-set-key [remap eval-last-sexp] 'sanityinc/eval-last-sexp-or-region)

;; Flycheck for emacs regular expressions
(use-package flycheck-relint)


(provide 'init-lisp)
;;; init-lisp.el ends here
