;;; init-php.el --- Support for working with PHP -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package php-mode
  :defer t)

(use-package smarty-mode
  :defer t
  :after php-mode)

(use-package company-php
  :defer t
  :after company php-mode
  :config (add-to-list 'company-backends 'company-ac-php-backend))

(provide 'init-php)
;;; init-php.el ends here
