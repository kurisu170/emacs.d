;;; init-elpa.el --- Settings and helpers for package.el  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'package)

;;; Install into separate package dirs for each Emacs version, to prevent bytecode incompatibility
(setq package-user-dir
      (expand-file-name (format "elpa-%s.%s" emacs-major-version emacs-minor-version)
                        user-emacs-directory))

(add-to-list 'package-archives '( "melpa" . "https://melpa.org/packages/") t)

(defmacro require-package (package &rest body)
  "Set up PACKAGE with rest BODY."
  (declare (indent 1))
  `(progn
     (if (require ,package nil 'noerror)
         (progn ,@body)
       (when (not (package-installed-p ,package))
         (condition-case ex
             (package-install ,package)
           (error nil)))
       (if (require ,package nil 'noerror)
           (progn ,@body)
         (display-warning 'require-package (format "Loading `%s' failed" ,package) :warning)))))

;;; Fire up package.el
(setq package-enable-at-startup nil)

(package-initialize)
(when (not package-archive-contents)
    (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

; (require 'use-package-ensure)
; (setq use-package-always-ensure t)


(let ((package-check-signature nil))
  (require-package 'gnu-elpa-keyring-update))


(provide 'init-elpa)
;;; init-elpa.el ends here
