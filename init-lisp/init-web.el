;;; init.el --- Load the full configuration         -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package web-mode
    :mode (("\\.html?$" . web-mode)
         ("\\.djhtml?$" . web-mode)
         ("\\.tsx$" . web-mode)
         ("\\.mustache\\'" . web-mode)
         ("\\.phtml?\\'" . web-mode)
         ("\\.as[cp]x\\'" . web-mode)
         ("\\.erb\\'" . web-mode)
         ("\\.hbs\\'" . web-mode))
    :custom ((web-mode-engines-alist '(("django" . "\\.html?\\'")))
             (web-mode-auto-close-style 2))
    :hook (web-mode . (lambda () (electric-pair-local-mode -1))))

(use-package emmet-mode
  :diminish
  :hook ((html-mode web-mode) . emmet-mode))


(provide 'init-web)
;;; init-web.el ends here
