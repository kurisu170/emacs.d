;;; init-yaml.el --- Support Yaml files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package highlight-indent-guides
  :diminish
  :custom ((highlight-indent-guides-method 'character)
           (highlight-indent-guides-auto-enabled nil)
           (highlight-indent-guides-responsive 'top)
           (highlight-indent-guides-delay 0.1))
  :config
  (set-face-background 'highlight-indent-guides-odd-face "darkgray")
  (set-face-background 'highlight-indent-guides-even-face "dimgray")
  (set-face-foreground 'highlight-indent-guides-character-face "dimgray"))

(use-package yaml-mode
  :hook (yaml-mode . highlight-indent-guides-mode)
  :mode "\\.yml\\.erb\\'")

(provide 'init-yaml)
;;; init-yaml.el ends here
