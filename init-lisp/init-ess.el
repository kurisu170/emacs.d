;;; init-ess.el --- ESS configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package ess)

(provide 'init-ess)
;;; init-ess.el ends here
