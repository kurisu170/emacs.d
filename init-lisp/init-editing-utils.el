;;; init-editing-utils.el --- Day-to-day editing helpers -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; Handy bindings
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "M-j") 'join-line)
(global-set-key (kbd "C-x C-.") 'pop-global-mark)

(autoload 'zap-up-to-char "misc" "Kill up to, but not including ARGth occurrence of CHAR.")
(global-set-key (kbd "M-z") 'zap-up-to-char)

;; Basic config
(setq-default
 bookmark-default-file (expand-file-name ".bookmarks.el" user-emacs-directory)
 column-number-mode t
 fill-column 80
 buffers-menu-max-size 30
 ediff-window-setup-function 'ediff-setup-windows-plain
 indent-tabs-mode nil
 tab-width 4
 create-lockfiles nil
 auto-save-default nil
 save-interprogram-paste-before-kill t
 scroll-preserve-screen-position 'always
 set-mark-command-repeat-pop t
 backward-delete-char-untabify-method 'all
 word-wrap t
 truncate-lines nil)

(setq line-move-visual nil)
(setf (cdr (assq 'continuation fringe-indicator-alist))
      '(nil right-curly-arrow))

;; Remove some useful commands from disabled
(dolist (command '(narrow-to-region
                   narrow-to-page
                   narrow-to-defun
                   upcase-region
                   downcase-region
                   set-goal-column))
  (put command 'disabled nil))
;; I don't use overwrite mode much so put it in disabled
(put 'overwrite-mode 'disabled t)


;; Smooth scrolling
(setq-default scroll-margin 7
              scroll-up-aggressively   0.01
              scroll-down-aggressively 0.01)

;; Abbreviations
(setq abbrev-file-name (expand-file-name "abbrev_defs" user-emacs-directory))
  (setq save-abbrevs 'silently)
  (set-default 'abbrev-mode t)
(diminish 'abbrev-mode)


;;----------------------------------------------------------------------------
;; Custom functions
;;----------------------------------------------------------------------------
;; Fix backward-up-list to understand quotes, see http://bit.ly/h7mdIL
(defun sanityinc/backward-up-sexp (arg)
  "Jump up to the start of the ARG'th enclosing sexp."
  (interactive "p")
  (let ((ppss (syntax-ppss)))
    (cond ((elt ppss 3)
           (goto-char (elt ppss 8))
           (sanityinc/backward-up-sexp (1- arg)))
          ((backward-up-list arg)))))
(global-set-key [remap backward-up-list] 'sanityinc/backward-up-sexp) ; C-M-u, C-M-up

(defun sanityinc/newline-at-end-of-line ()
  "Move to end of line, enter a newline, and reindent."
  (interactive)
  (move-end-of-line 1)
  (newline-and-indent))
(global-set-key (kbd "S-<return>") 'sanityinc/newline-at-end-of-line)

(defun kill-back-to-indentation ()
  "Kill from point back to the first non-whitespace character on the line."
  (interactive)
  (let ((prev-pos (point)))
    (back-to-indentation)
    (kill-region (point) prev-pos)))
(global-set-key (kbd "C-M-<backspace>") 'kill-back-to-indentation)

;;----------------------------------------------------------------------------
;; Packages
;;----------------------------------------------------------------------------
;; Useful minor modes that dont require any config
(require-package 'whole-line-or-region)
(require-package 'highlight-escape-sequences)
(dolist (mode '(electric-pair-mode
                electric-indent-mode
                global-so-long-mode
                global-auto-revert-mode
                transient-mark-mode
                global-auto-revert-mode
                global-subword-mode
                whole-line-or-region-global-mode
                save-place-mode
                global-hl-line-mode
                hes-mode
                show-paren-mode))
  (add-hook 'after-init-hook mode))

(eval-after-load "whole-line-or-region"
  '(diminish 'whole-line-or-region-local-mode))
(eval-after-load "subword"
  '(diminish 'subword-mode))


;; Various highlighting and visual feedback
(use-package goto-line-preview)

(use-package rainbow-delimiters
  :defer t
  :hook (prog-mode . rainbow-delimiters-mode))

;; Interface tweaks
(use-package display-line-numbers
  :custom (display-line-numbers-width 3)
  ;; Disable line numbers for some modes
  :hook ((occur-mode
          erc-mode
          term-mode
          eshell-mode
          vterm-mode
          neotree-mode
          telega-char-mode
          telega-root-mode
          telega-webpage-mode
          dashboard-mode) . (lambda () (display-line-numbers-mode -1)))
  :config
  (global-display-line-numbers-mode))

(use-package undo-tree
  :defer nil
  :diminish
  :ensure t
  :config
  (global-undo-tree-mode))

(use-package display-fill-column-indicator
  :defer t
  :hook (prog-mode . display-fill-column-indicator-mode))

;; (use-package which-key
;;   :diminish
;;   :custom (which-key-idle-delay 1)
;;   :config
;;   (which-key-setup-minibuffer)
;;   (which-key-mode))


(use-package avy
  :defer t
  :custom ((avy-keys '(?a ?r ?s ?t ?n ?e ?i ?o))
           (avy-all-windows nil))
  :bind ("C-," . avy-goto-char-timer))

(use-package expand-region
  :bind (("C-." . er/expand-region)))


(provide 'init-editing-utils)
;;; init-editing-utils.el ends here
