;;; init-windows.el --- Working with windows within frames -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package ace-window
  :custom (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :bind (("M-o" . ace-window)))

;;----------------------------------------------------------------------------
;; Automaticly resize windows to show at least 80 columns
;;----------------------------------------------------------------------------
(use-package zoom
  :diminish
  :custom (zoom-size '(80 . 0.1))
  :config (zoom-mode))


(provide 'init-windows)
;;; init-windows.el ends here
