;;; init-ctrlf.el --- Ctrlf config                   -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package ctrlf
  :config (ctrlf-mode))

(provide 'init-ctrlf)

;;; init-ctrlf.el ends here
