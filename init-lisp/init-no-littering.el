;;; init-no-littering.el --- Clean up emacs folders  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package no-littering)

(provide 'init-no-littering)
;;; init-no-littering.el ends here
