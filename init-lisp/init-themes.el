;;; init-themes.el --- Theme config                  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(setq custom-safe-themes t)

(use-package solarized-theme
  :custom ((solarized-use-variable-pitch nil)
           (solarized-height-minus-1 1.0)
           (solarized-height-plus-1 1.0)
           (solarized-height-plus-2 1.0)
           (solarized-height-plus-3 1.0)
           (solarized-height-plus-4 1.0))
  :config
  (load-theme 'solarized-dark-high-contrast)

  (defun light ()
    "Activate a light color theme."
    (interactive)
    (load-theme 'solarized-light))

  (defun dark ()
    "Activate a dark color theme."
    (interactive)
    (load-theme 'solarized-dark-high-contrast)))


(provide 'init-themes)
;;; init-themes.el ends here
