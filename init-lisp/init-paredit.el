;;; init-paredit.el --- Configure paredit structured editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package paredit
  :diminish
  :bind (:map paredit-mode-map
              ("RET" . paredit-newline)
              ;; Suppress certain paredit keybindings to avoid clashes
              ("C-<left>" . nil)
              ("C-<right>" . nil)
              ("C-M-<left>" . nil)
              ("C-M-<right>" . nil)
              ("M-s" . nil)))

(provide 'init-paredit)
;;; init-paredit.el ends here
