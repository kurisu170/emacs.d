;;; init-dired.el --- Short description -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package dired
  :hook (dired-mode . dired-hide-details-mode)
  :custom ((dired-recursive-copies 'always)
           (dired-recursive-deletes 'always)
           (dired-listing-switches
            "-AGFXhlv --group-directories-first")))
(use-package dired-subtree
  :custom (dired-subtree-use-backgrounds nil)
  :bind (:map dired-mode-map
              ("<tab>" . dired-subtree-toggle)
              ("<C-tab>" . dired-subtree-cycle)))


(provide 'init-dired)
;;; init-dired.el ends here
