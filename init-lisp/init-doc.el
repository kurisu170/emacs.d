;;; init-helpful.el --- Setup for various documentation tools -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(setq Info-additional-directory-list
      (list (expand-file-name "~/.local/share/info")))

(use-package info-colors
  :hook (Info-selection . info-colors-fontify-node))

(use-package helpful
  :bind (("C-h f" . helpful-callable)
         ("C-h v" . helpful-variable)
         ("C-h k" . helpful-key)))


(provide 'init-doc)
;;; init-doc.el ends here
