;;; init-git.el --- Git SCM support -*- lexical-binding: t -*-
;;; Commentary:

;; See also init-github.el.

;;; Code:
(autoload 'smerge-mode "smerge-mode" nil t)

(require-package 'git-blamed)
(require-package 'gitignore-mode)
(require-package 'gitconfig-mode)

(use-package magit
  :bind (("C-x M-g" . magit-dispatch)
         ("C-x g" . magit-status)))


;; Work with dotfiles stored using bare repository from magit
(defvar dotfiles-git-dir
  (concat "--git-dir=" (expand-file-name "~/.dotfiles")))
(defvar dotfiles-work-tree
  (concat "--work-tree=" (expand-file-name "~")))

(require 'magit)
(defun my/dotfiles-status-or-magit-status (&optional arg)
  "Open `magit-status' on dotfiles if ARG is not nil or run `magit-status'."
  (interactive "P")
  (if arg
      (let ((magit-git-global-arguments
             (append (list dotfiles-git-dir dotfiles-work-tree)
                     magit-git-global-arguments)))
        (magit-status-setup-buffer dotfiles-work-tree))
    (magit-status-setup-buffer)))
(global-set-key (kbd "C-x g") 'my/dotfiles-status-or-magit-status)


(provide 'init-git)
;;; init-git.el ends here
