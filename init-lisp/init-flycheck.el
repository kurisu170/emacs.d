;;; init-flycheck.el --- Configure flycheck global behavior  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package flycheck
  :diminish
  :custom (flycheck-display-errors-function
           #'flycheck-display-error-messages-unless-error-list)
  :config (global-flycheck-mode))


(provide 'init-flycheck)
;;; init-flycheck.el ends here
