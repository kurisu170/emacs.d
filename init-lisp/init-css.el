;;; init-css.el --- CSS/Less/SASS/SCSS support -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;;; Colorize CSS colour literals
(use-package rainbow-mode
  :ensure t
  :hook ((web-mode css-mode html-mode sass-mode) . rainbow-mode))

(use-package sass-mode)
(use-package scss-mode
  :custom (scss-compile-at-save nil))

(use-package less-css-mode)
(use-package skewer-less
  :after less-css-mode
  :hook (less-css-mode . skewer-less-mode))

(use-package skewer-mode
  :after css-mode
  :hook (css-mode . skewer-css-mode))

;;; Use eldoc for syntax hints
(use-package css-eldoc
  :hook (css-mode-hook . turn-on-css-eldoc))


(provide 'init-css)
;;; init-css.el ends here
