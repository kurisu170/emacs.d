;;; init-shell.el --- Shell configaration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(defun my/comint-send-input-maybe ()
  "Only `comint-send-input' when point is after the latest prompt.

Otherwise move to the end of the buffer."
  (interactive)
  (let ((proc (get-buffer-process (current-buffer))))
    (if (and proc (>= (point) (marker-position (process-mark proc))))
        (comint-send-input)
      (goto-char (point-max)))))

(with-eval-after-load "comint"
  (global-set-key [remap comint-send-input] 'my/comint-send-input-maybe))

(global-set-key (kbd "C-c s") 'shell)


(provide 'init-shell)
;;; init-shell.el ends here
