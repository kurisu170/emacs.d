;;; init-org.el --- Org-mode config                  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
(require 'org-tempo)

(use-package org
  :diminish 'org-indent-mode
  :hook ((org-mode . org-indent-mode)
         (org-mode . toggle-truncate-lines))
  :custom
  ((org-directory (expand-file-name "~/.local/share/org"))
   (org-src-tab-acts-nativaly t)
   (org-indent-indentation-per-level 1)
   (org-hide-leading-start t)
   (org-ellipsis " ⬎")
   (org-confir-babel-evaluate nil)
   (org-babel-load-languages '((emacs-lisp . t)
                               (python . t)
                               (shell . t)))

   (org-structure-template-alist (append '(("sh" . "src shell")
                                           ("el" . "src emacs-lisp")
                                           ("py" . "src python")
                                           ("r" . "src http"))
                                         org-structure-template-alist))
   (org-capture-templates
    `(("d" "day" entry (file "day.org")
       "* %t\n** %?")
      ("t" "todo" entry (file "todo.org")
       "* TODO %?\n%U\n" :clock-resume t)
      ("n" "note" entry (file "notes.org")
       "* %U\n%?" :clock-resume t)
      ("o" "order" table-line (file "orders.org")
       "|%?||||")
      ))
   (org-agenda-files (list org-directory)))


  :bind (("C-c c" . org-capture)
         :map org-mode-map
         ("C-M-<return>" . org-insert-todo-heading))
  :config (add-to-list 'org-export-backends 'babel))

(use-package org-roam
  :custom ((org-roam-directory (expand-file-name "~/.local/share/roam")))
  :bind (("C-c r" . org-roam-find-file))

;; HTTP requests in org-babel blocks
(use-package ob-http
  :after org)

(provide 'init-org)
;;; init-org.el ends here
