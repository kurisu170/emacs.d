;;; init-selectrum.el --- Config for selectrum       -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


(use-package selectrum
  :defer nil
  :custom (selectrum-fix-vertical-window-height t)
  :bind (:map selectrum-minibuffer-map
              ("C-<backspace>" . backward-kill-sexp))
  :config
  (selectrum-mode))

(use-package prescient
  :after selectrum
  :config (prescient-persist-mode))

(use-package selectrum-prescient
  :after selectrum prescient
  :config (selectrum-prescient-mode))

(use-package embark
  :defer t
  :bind (:map selectrum-minibuffer-map
              ("C-c C-o" . embark-export)
              ("C-c C-c" . embark-act))
  :config
  (if (package-installed-p 'which-key)
      (setq embark-action-indicator
      (lambda (map _target)
        (which-key--show-keymap "Embark" map nil nil 'no-paging)
        #'which-key--hide-popup-ignore-command)
      embark-become-indicator embark-action-indicator)))

(use-package consult
  :custom ((xref-show-xrefs-function #'consult-xref)
           (xref-show-definitions-function #'consult-xref))
  :config
  (if (package-installed-p 'projectile)
    (setq consult-project-root-function 'projectile-project-root)))

(use-package marginalia
  :after selectrum
  :custom (marginalia-annotators '(marginalia-annotators-heavy))
  :config (marginalia-mode))


(provide 'init-selectrum)
;;; init-selectrum.el ends here
