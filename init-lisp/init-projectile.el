;;; init-projectile.el --- Use Projectile for navigation within projects -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package projectile
  :diminish
  :bind-keymap ("C-c p" . projectile-command-map)
  :custom (projectile-mode-line-prefix " Proj")
  :config
  (if (file-directory-p "~/.local/src/")
    (setq projectile-project-search-path '("~/.local/src/")))
  (when (executable-find "rg")
    (setq-default projectile-generic-command "rg --files --hidden"))
  (projectile-mode))

(provide 'init-projectile)
;;; init-projectile.el ends here
