;;; init.el --- Load the full configuration         -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; This file bootstraps the configuration, which is divided into
;; a number of other files.

;; This emacs config is heavely inspired by configuration of Steve Purcell
;; which you can find at https://github.com/purcell/emacs.d

;; "lisp" is for all my custom libraries
;; "init-lisp" is for init customizations;
;; "contrib-lisp" is for third-party code that I handle manually
(dolist (path '("lisp" "init-lisp" "contrib-lisp"))
  (add-to-list 'load-path (locate-user-emacs-file path)))
;;----------------------------------------------------------------------------
;; Bootstrap config
;;----------------------------------------------------------------------------
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(require 'init-elpa)      ;; Machinery for installing required packages

;;----------------------------------------------------------------------------
;; Load configs for specific features and modes
;;----------------------------------------------------------------------------
(require-package 'diminish)

(require 'init-no-littering)

(require 'init-themes)
(require 'init-gui-frames)

(require 'init-dired)
;; (require 'init-isearch)
(require 'init-ctrlf)
;; (require 'init-grep)
(require 'init-uniquify)
;; (require 'init-ibuffer)
(require 'init-flycheck)

;; (require 'init-recentf)
(require 'init-selectrum)
(require 'init-hippie-expand)
;; (require 'init-company)
(require 'init-lsp)
(require 'init-windows)
;; (require 'init-sessions)
;; (require 'init-mmm)

(require 'init-doc)
(require 'init-shortcuts)
(require 'init-editing-utils)
(require 'init-shell)
(require 'init-whitespace)
(require 'init-direnv)

(require 'init-vc)
(require 'init-git)
(require 'init-forge)

(require 'init-projectile)

;; (require 'init-compile)
(require 'init-crontab)
;; (require 'init-textile)
(require 'init-ess)
(require 'init-markdown)
(require 'init-csv)
(require 'init-javascript)
(require 'init-php)
(require 'init-org)
;; (require 'init-nxml)
;; (require 'init-html)
(require 'init-http)
(require 'init-web)
(require 'init-css)
;; (require 'init-haml)
(require 'init-python)
(require 'init-yaml)
(use-package nginx-mode)

(require 'init-paredit)
(require 'init-lisp)
(require 'init-geiser)

(require 'init-telega)

;; (require 'init-evil)
;; Extra packages which don't require much configuration

(use-package eldoc
  :defer nil
  :diminish
  :config (global-eldoc-mode))


(provide 'init)
;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
;;; init.el ends here
